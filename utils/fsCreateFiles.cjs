const path = require('path');
const fs = require("fs");
const createError = require("http-errors");

function fsCreateFiles(folderName, files){

    return new Promise((resolve, reject) => {
        let createPromises = files.map((currFile) => {
            return new Promise((resolve, reject) => {
                if (typeof currFile !== "string" || currFile.trim().length === 0 || currFile.includes("/")){
                    reject(createError(400, "Invalid Filename"));
                }
                else {
                    let file = `${currFile}.json`;
                    fs.open(path.join(__dirname, "..", "public", folderName, file), "w", (err) => {
                        if(err){
                            reject(err);
                        } else {
                            resolve(file);
                        }
                    });
                }   
            });
        });

        Promise.allSettled(createPromises)
        .then((results) => {
            const allFulfilled = results.filter((currPromise) => {
                return currPromise.status === 'fulfilled';
            })
            .map((currPromise) => {
                return currPromise.value;
            });
            resolve(allFulfilled);
        })
    });
}

module.exports = fsCreateFiles;