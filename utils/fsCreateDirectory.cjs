const path = require('path');
const fs = require("fs");
const createError = require("http-errors");

function fsCreateDirectory(folderName){
    return new Promise((resolve, reject) => {
        if(folderName.trim().length === 0){
            reject(createError(400, "Directory name empty"));
        }
        else if(folderName.includes("/")){
            reject(createError(400, "Can't include / in directory name"));
        }
        else {
            fs.mkdir(path.join(__dirname, "..", "public", folderName), {recursive : true} ,(err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(folderName);
                }
            });
        }
    });
}

module.exports = fsCreateDirectory;