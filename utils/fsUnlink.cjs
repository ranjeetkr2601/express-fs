const path = require('path');
const fs = require("fs");
const createError = require("http-errors");

function fsUnlink (folder, files) {
    return new Promise((resolve, reject) => {

        if (folder.trim().length === 0 || folder.includes("/")){
            reject(createError(400, "Invalid folder name"));
        }
        else {
            let deletePromises = files.map((currFile) => {
                return new Promise((resolve, reject) => {
                    if(typeof currFile !== "string" || currFile.trim().length === 0 || currFile.includes("/")){
                        reject(createError(400, "Invalid filename"));
                    }
                    else {
                        let file = `${currFile}.json`;
                        fs.unlink(path.join(__dirname, "..", "public", folder, file), (err) => {
                            if (err){
                                reject(err);
                            }
                            else {
                                resolve(file);
                            }
                        }); 
                    }
                });
            });
    
            Promise.allSettled(deletePromises)
            .then((results) => {
                const allFulfilled = results.filter((currPromise) => {
                    return currPromise.status === 'fulfilled';
                })
                .map((currPromise) => {
                    return currPromise.value;
                });
                resolve(allFulfilled);
            });
        }
    });
}

module.exports = fsUnlink;