function errorHandler(err, req, res, next) {

    if (err.code === "ENOENT"){
        res.status(500).json({
            message : "No such file or directory"
        }).end();
    }
    else if (err.code === "EEXIST"){
        res.status(500).json({
            message : "Directory already exists"
        }).end();
    }
    else if (err.code === "EACCES"){
        res.status(403).json({
            message : "Permission Denied"
        }).end();
    }
    else {
        res.status(400).json(err).end();
    } 
}

module.exports = errorHandler;