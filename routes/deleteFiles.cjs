const express = require('express');
const createError = require("http-errors");

const fsUnlink = require("../utils/fsUnlink.cjs");

const router = express.Router();
router.use(express.json());

router.delete("/", (req, res, next) => {

    if(req.headers['content-type'] !== "application/json"){
        next(createError(400, "Content not in json format"));
    }
    else {
        let directory = req.body.directory;
        let files = req.body.files;
    
        if (typeof directory === "string" && Array.isArray(files)){
            fsUnlink(directory, files)
            .then((deletedFiles) => {
                if (deletedFiles.length === files.length){
                    res.status(200).json({
                        "deletedFiles" : deletedFiles
                    }).end();
                } 
                else if (deletedFiles.length === 0){
                    next(createError(400, "Invalid file names/ File Not found"));
                }
                else {
                    res.status(206).json({
                        "deletedFiles" : deletedFiles
                    }).end();
                }  
            })
            .catch((err) => {
                next(err);
            });
        }
        else {
            next(createError(400, {
                message : "Invalid delete request",
                acceptedFormat : {
                        directory : "string",
                        files : "array of strings"
                    }
                }));
        }
        
    }
});

module.exports = router;