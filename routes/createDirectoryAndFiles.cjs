const express = require('express');
const createError = require("http-errors");

const fsCreateDirectory = require("../utils/fsCreateDirectory.cjs");
const fsCreateFiles = require("../utils/fsCreateFiles.cjs");

const router = express.Router();
router.use(express.json());

router.post("/", (req, res, next) => {

    if(req.headers['content-type'] !== "application/json"){
        next(createError(400, "Content not in json format"));
    }
    else {
        let directory = req.body.directory;
        let files = req.body.files;

        if(typeof directory === 'string' && Array.isArray(files)){

            fsCreateDirectory(directory)
            .then((folderName) => {
                return fsCreateFiles(folderName, files);
            })
            .then((createdFiles) => {
                if(createdFiles.length === files.length){
                    res.status(200).json({
                        "createdFiles" : createdFiles
                    }).end();;
                }
                else if(createdFiles.length === 0){
                    next(createError(400, "Invalid file name/format"));
                }
                else {
                    res.status(206).json({
                        "createdFiles" : createdFiles
                    }).end();
                }
            })
            .catch((err) => {
                next(err);
            });
        }
        else {
            next(createError(400, {
                message : "Invalid post request",
                acceptedFormat : {
                        directory : "string",
                        files : "array of strings"
                    }
                }));
        } 
    } 
});

module.exports = router;