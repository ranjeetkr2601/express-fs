const express = require('express');

const errorHandler = require('./middlewares/errorHandler.cjs');
const createDirectoryAndFiles = require("./routes/createDirectoryAndFiles.cjs");
const deleteFiles = require("./routes/deleteFiles.cjs");

const app = express();
const PORT =  process.env.PORT || 8000;

app.use("/create", createDirectoryAndFiles);

app.use("/delete", deleteFiles);

app.all("*", (req, res) => {
    res.status(404).json({
        error : "Not found"
    }).end();
});

app.use(errorHandler);

app.listen(PORT, () => {
    console.log(`Server listening to port: ${PORT}`);
});